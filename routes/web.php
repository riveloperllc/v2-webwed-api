<?php
// header('Access-Control-Allow-Origin: *');
// header('Access-Control-Allow-Headers: *');
// header('Access-Control-Allow-Methods: *');
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    Auth::loginUsingId(1, true);
    return dd(Auth::user());
    return view('welcome');
});

// --------------------
// Backpack\Demo routes
// --------------------
Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['admin'],
    'namespace'  => 'Admin',
], function () {
    // CRUD resources and other admin routes
    CRUD::resource('monster', 'MonsterCrudController');
    CRUD::resource('user', 'FirebaseUsersCrudController');
    // CRUD::resource('admins', 'UserCrudController');
    CRUD::resource('officiants', 'OfficiantsCrudController');
    CRUD::resource('marriagelicense', 'MarriageLicenseCrudController');
    CRUD::resource('marriageapplication', 'MarriageApplicationCrudController');
    CRUD::resource('coupons', 'CouponsCrudController');

    CRUD::resource('events', 'EventsCrudController');
    CRUD::resource('event_options', 'Event_optionsCrudController');

    CRUD::resource('event_sales', 'Event_salesCrudController');
    CRUD::resource('preimum_addon_sales', 'Preimum_addon_salesCrudController');

    CRUD::resource('packages', 'PackagesCrudController');
    CRUD::resource('preimum_addons', 'Premium_addonsCrudController');

    CRUD::resource('emojis', 'EmojisCrudController');
    CRUD::resource('officiant_questions', 'OfficiantQuestionsCrudController');
    CRUD::resource('officiantquestions', 'OfficiantQuestionsCrudController');
    CRUD::resource('courts', 'CourtsCrudController');
    CRUD::resource('eventroles', 'EventRolesCrudController');
});

Route::get('api/article', 'Api\ArticleController@index');
Route::get('api/article/{id}', 'Api\ArticleController@show');
