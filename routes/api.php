<?php
// header('Access-Control-Allow-Origin: *');
// header('Access-Control-Allow-Headers: *');
// header('Access-Control-Allow-Methods: *');
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::match(['get','post'], '/auth/login', 'Api\NewAuthenticationController@login')->middleware('cors', 'cors-preflight');

Route::match(['get', 'post'], '/', function(Request $request){
  echo 'WEB WED MOBILE API V2';
});

//All Event Routes

Route::get('/event/config', 'Api\EventController@getEventConfigs')->middleware('cors', 'cors-preflight');
Route::post('/event/create', 'Api\EventController@createEvent')->middleware('api-auth', 'cors', 'cors-preflight');
Route::put('/event/create', 'Api\EventController@createEvent')->middleware('api-auth', 'cors', 'cors-preflight');
Route::post('/event/{id}/send_invite', 'Api\EventController@configureInvite')->middleware('api-auth', 'cors', 'cors-preflight');
Route::get('/event/{id}/invite', 'Api\EventController@inviteGuests')->middleware('api-auth', 'cors', 'cors-preflight');
Route::get('/event/{id}/invite/{response}', 'Api\EventController@respondToInvite')->middleware('api-auth', 'cors', 'cors-preflight');
Route::get('/event/{id}/{type?}', 'Api\EventController@getEvent')->middleware('cors', 'cors-preflight');
Route::get('/public/{id}/{privacy?}', 'Api\EventController@getEventPrivacy')->middleware('cors', 'cors-preflight');
Route::post('/event/{id}/participants/add', 'Api\EventController@addParticipant')->middleware('api-auth', 'cors', 'cors-preflight');
Route::post('/event/{id}/participants/{participantId}/update', 'Api\EventController@updateParticipant')->middleware('api-auth', 'cors', 'cors-preflight');
Route::post('/event/{id}/participants/{participantId}/remove', 'Api\EventController@removeParticipant')->middleware('api-auth', 'cors', 'cors-preflight');
Route::post('/event/{id}/guests/update', 'Api\EventController@updateGuests')->middleware('api-auth', 'cors', 'cors-preflight');
Route::get('/event/share/{id}', function(){ return 'EVENT SHARE DEEPLINK'; })->middleware('cors', 'cors-preflight');
Route::get('/start/{id}/{userId}', 'Api\EventController@startEvent')->middleware('cors', 'cors-preflight');
Route::get('/archive/{id}', 'Api\EventController@getArchive')->middleware('cors', 'cors-preflight');
Route::get('/stream/{id}', 'Api\EventController@getMinutes')->middleware('cors', 'cors-preflight');
Route::get('/stop_stream/{id}/{userId}', 'Api\EventController@stopEvent')->middleware('cors', 'cors-preflight');
Route::get('/emojis', 'Api\EventController@getEmojis')->middleware('cors', 'cors-preflight');
Route::get('/event/{id}/stream', 'Api\EventController@streamEvent')->middleware('cors', 'cors-preflight');

//Webhooks
Route::post('/webhooks/connectionCreated', 'Api\EventController@connectionCreated')->middleware('cors', 'cors-preflight');
Route::post('/webhooks/connectionDestroyed', 'Api\EventController@connectionDestroyed')->middleware('cors', 'cors-preflight');
Route::post('/webhooks/streamCreated', 'Api\EventController@streamCreated')->middleware('cors', 'cors-preflight');
Route::post('/webhooks/streamDestroyed', 'Api\EventController@streamDestroyed')->middleware('cors', 'cors-preflight');


//All User Routes

Route::get('/user/{id}', 'Api\NewUserController@getUser')->middleware('api-auth', 'cors', 'cors-preflight');
Route::get('/user/{id}/events', 'Api\EventController@getEvents')->middleware('api-auth', 'cors', 'cors-preflight');
Route::get('/user/{id}/orders', 'Api\OrderController@getOrders')->middleware('api-auth', 'cors', 'cors-preflight');
Route::get('/user/{id}/notifications', 'Api\NewUserController@getNotifications')->middleware('api-auth', 'cors', 'cors-preflight');
Route::get('/user/{id}/notification/{notificationid?}/{type?}', 'Api\NewUserController@getNotifications')->middleware('api-auth', 'cors', 'cors-preflight');
Route::get('/user/{id}/avatar', 'Api\NewUserController@getAvatar')->middleware('cors', 'cors-preflight');
Route::get('/officiants', 'Api\NewUserController@listOfficiants')->middleware('cors', 'cors-preflight');
Route::post('/myaccount/update', 'Api\NewUserController@updateAccount')->middleware('api-auth', 'cors', 'cors-preflight');
Route::get('/myaccount/payment_info', function(){
  return response()->json([
    "success" => true,
    "card" => array(
      "number" => "****************1234",
      "name" => "Austin Sweat",
      "expires" => strtotime("now"),
      "ccv" => "123",
      "zip" => "30052"
    )
  ]);
});
Route::get('/steps', function(){
  return response()->json([
    "success" => true,
    "steps" => array(

    )
  ]);
});
Route::get('/test', 'Api\EventController@test');

Route::post('/marriage/application', function(Request $request){
  return response()->json([
    'success' => true,
    'data' => array(
      'submitted' => false,
      'history' => array(
        array(
          'title' => 'Application Submitted',
          'date' => strtotime('now')
        )
      ),
      'attachments' => array(
        array(
          'id' => 1,
          'title' => 'Birth certificate',
          'date' => strtotime('now')
        )
      )
    )
  ]);
})->middleware('cors', 'cors-preflight');

Route::post('/marriage/application/attachment/add', function(Request $request){
  return response()->json([
    'success' => true,
    'data' => array(
      'submitted' => false,
      'history' => array(
        array(
          'title' => 'Application Submitted',
          'date' => strtotime('now')
        )
      ),
      'attachments' => array(
        array(
          'id' => 1,
          'title' => 'Birth certificate',
          'date' => strtotime('now')
        )
      )
    )
  ]);
})->middleware('api-auth', 'cors', 'cors-preflight');

Route::post('/marriage/application/attachment/remove', function(Request $request){
  return response()->json([
    'success' => true,
    'data' => array(
      'submitted' => false,
      'history' => array(
        array(
          'title' => 'Application Submitted',
          'date' => strtotime('now')
        )
      ),
      'attachments' => array(
        array(
          'id' => 1,
          'title' => 'Birth certificate',
          'date' => strtotime('now')
        )
      )
    )
  ]);
})->middleware('api-auth', 'cors', 'cors-preflight');

Route::post('/marriage/application/submit', function(Request $request){
  return response()->json([
    'success' => true,
    'data' => array(
      'submitted' => true,
      'history' => array(
        array(
          'title' => 'Application Submitted',
          'date' => strtotime('now')
        )
      ),
      'attachments' => array(
        array(
          'id' => 1,
          'title' => 'Birth certificate',
          'date' => strtotime('now')
        )
      )
    )
  ]);
})->middleware('api-auth', 'cors', 'cors-preflight');

Route::post('/marriage/license', function(Request $request){
  return response()->json([
    'success' => true,
    'data' => array(
      'submitted' => false,
      'signers' => array(
        array(
          "user" => 1,
          "name" => "John Doe",
          "identifier" => "johndoe@webwedmobile.com",
          "response" => array("color" => "green", "text" => "SIGNED")
        )
      )
    )
  ]);
})->middleware('cors', 'cors-preflight');

Route::post('/marriage/license/signer/add', function(Request $request){
  return response()->json([
    'success' => true,
    'data' => array(
      'submitted' => false,
      'signers' => array(
        array(
          "user" => 1,
          "name" => "John Doe",
          "identifier" => "johndoe@webwedmobile.com",
          "response" => array("color" => "green", "text" => "SIGNED")
        )
      )
    )
  ]);
})->middleware('api-auth', 'cors', 'cors-preflight');

Route::post('/marriage/license/signer/remove', function(Request $request){
  return response()->json([
    'success' => true,
    'data' => array(
      'submitted' => false,
      'signers' => array(
        array(
          "user" => 1,
          "name" => "John Doe",
          "identifier" => "johndoe@webwedmobile.com",
          "response" => array("color" => "green", "text" => "SIGNED")
        )
      )
    )
  ]);
})->middleware('api-auth', 'cors', 'cors-preflight');

Route::post('/marriage/license/submit', function(Request $request){
  return response()->json([
    'success' => true,
    'data' => array(
      'submitted' => true,
      'signers' => array(
        array(
          "user" => 1,
          "name" => "John Doe",
          "identifier" => "johndoe@webwedmobile.com",
          "response" => array("color" => "green", "text" => "SIGNED")
        )
      )
    )
  ]);
})->middleware('api-auth', 'cors', 'cors-preflight');
