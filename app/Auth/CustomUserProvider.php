<?php namespace App\Auth;

use Illuminate\Contracts\Auth\UserProvider as UserProviderInterface;
use Illuminate\Contracts\Auth\Authenticatable;

use Vinkas\Firebase\Auth\User;

class CustomUserProvider implements UserProviderInterface {
	protected $table = 'firebase_users';
	protected $model;

	public function __construct($model)
	{
		$this->model = $model;
	}

	public function retrieveById($identifier)
	{
		$user = $this->model->where('uid','=',$identifier);
		if ($user->exists()){
			$user = $user->first();
			return $user;
		}else{
			return null;
		}
	}

	public function retrieveByToken($identifier, $token)
	{

	}

	public function updateRememberToken(Authenticatable $user, $token)
	{

	}

	public function retrieveByCredentials(array $credentials)
	{

	}

	public function validateCredentials(Authenticatable $user, array $credentials)
	{

	}

}
