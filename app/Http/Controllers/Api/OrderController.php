<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Users;

class OrderController extends Controller
{
  /**
   * getOrders is for grabbing a users orders.
   * @param  string  $id  User ID not UID!
   * @return Response     JSON Response is provided.
   */
  public function getOrders($id){
    if ($this->isMyAccount(Auth::user()->id, $id)){
      $user = Users::find(Auth::user()->uid);

      $orders = array();
      $_orders = $user->orders;
      $tmpOrders = array();
      foreach($_orders as $order){
        $tmpOrders = array_merge($tmpOrders, array(
            $order
          )
        );
      }

      usort($tmpOrders, function($a, $b) {
           return strtotime($b['created_at']) - strtotime($a['created_at']);
      });

      foreach($tmpOrders as $order){
        $event = $order->_event;
        $package = $event->_package;


        $orders = array_merge($orders, array(
          array(
              "title" => $event->title,
              "date" => strtotime($order->created_at),
              "price" => $order->price_paid,
              "details" => array(
                "package" => array(
                  "title" => $package->getTitle($package->time_alotment),
                  "price" => $package->price
                ),
                "addons" => array()
              )
            )
          )
        );
      }

      //return dd($user->orders[0]->_event->_package);
      return response()->json(array(
          "success" => true,
          "data" => array(
            "orders" => $orders
          )
        )
      );
    }else{
      return response()->json(array(
        "success" => false,
        "data" => array()
      ));
    }
  }

  public function isMyAccount($myId, $otherId){
    if ($myId == $otherId){
      return true;
    }else{
      return false;
    }
  }
}
