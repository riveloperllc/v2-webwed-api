<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Events;
use App\Models\Users;
use Auth;
use App\Models\Images;
use App\Models\OpenTokController;
use App\Models\EventRoles;
use App\Models\Packages;
use App\Models\Premium_addons;
use App\Models\Event_options;
use App\Models\Invites;
use App\Models\Emojis;
use App\Models\StripeController;
use Illuminate\Support\Facades\Log;


class EventController extends Controller
{
    public function __construct(){

    }

    /**
     * getEvent provides an interface for getting an event with an id and type.
     * @param  int $id      Public id of the event.
     * @param  string $type minimal or detailed
     * @return Response       Returns Laravel JSON Response or array if requested.
     */
    public function getEvent($id, $type = 'minimal', $wantsArray = false){
      $event = Events::find($id);

      if ($wantsArray){
        return $event->makeArray($type);
      }else{
        return response()->json(array(
          'success' => true,
          'data' => $event->makeArray($type)
        ));
      }
    }

    public function getEvents($id, Request $request){
      $user = Users::find(Auth::user()->uid);

      if ($id == $user->id){
        return response()->json([
          'success' => true,
          'data' => array(
            "future" => $user->getEvents($user, false),
            "past" => $user->getEvents($user, true)
          )
        ]);
      }else{
        return response()->json([
          'success' => false,
          'data' => array()
        ]);
      }
    }

    public function getEventConfigs(Request $request){
      $addons = Premium_addons::all();
      $availableAddons = array();

      foreach($addons as $addon){
        $availableAddons = array_merge($availableAddons, array(
            array(
              "id" => $addon->id,
              "title" => $addon->title,
              "price" => $addon->price,
              "name" => $addon->title
            )
          )
        );
      }

      //Disabled Premium addons

      return response()->json(array(
          'success' => true,
          'data' => array(
            'packages' => (new Packages())->getPackages(),
            'addons' => $availableAddons,
            'roles' => EventRoles::get()
          )
        )
      );
    }

    private function generateEventCode(){
      return mt_rand(9999,99999);
    }

    public function createEvent(Request $request){
      //Normal request data
      //N
      // return response()->json(array(
      //   'file' => $request->file('file'),
      //   'fields' => $request->all()
      // ));
      //
      // return response()->json($request->all());

      //@TODO implement converting fields date, and time to a single timestamp in UTC based on users timestamp.

      $data = array(
        'photo' => ($request->has('photo') ? $request->input('photo') != '' ? $request->input('photo') : 'assets/images/Placeholder.svg' : 'assets/images/Placeholder.svg'),
        'title' => $request->input('title'),
        'date' => strtotime(($request->has('date') ? date('m/d/Y', $request->input('date')) : '') . ' ' . ($request->has('time') ? date('h:i A', $request->input('time')) : '')),
        'package' => $request->input('package'),
        'addons' => $request->input('addons')
      );

      $stripeToken = $request->input('token');
      $uid = Auth::user()->uid;
      $package = Packages::find($data['package']['id']);
      $transaction = (new StripeController)->makePayment($package->price, $stripeToken);


      //Create Event Instance
      $event = new Events([
        'title' => $data['title'],
        'event_code' => $this->generateEventCode(),
        'uid' => $uid,
        'package' => $data['package']['id'],
        'scheduled_date' => $data['date'],
        'status' => 1
      ]);

      //Save prior to creating sale record
      $event->save();

      //Save image
      if ($data['photo']){
        $image = new Images([
          'name' => 'event'.$event->id,
          'value' => $data['photo']
        ]);
        $image->save();
      }

      //Create Sale Record
      //Notify webwed of sale record
      $event->_saleRecord()->create([
        'uid' => $uid,
        'price_paid' => $transaction['price'],
        'txn_id' => $transaction['txn_id'],
        'coupon_code' => (@$transaction['coupon'] ? @$transaction['coupon'] : '')
      ]);

      foreach ($data['addons'] as $addon){
        //add each addon sale record
      }

      //Add user as participant
      $event->_participants()->create([
        'uid' => $uid,
        'role' => 'creator'
      ]);

      $event->_options()->create([
        'option_key' => 'remaining_time',
        'option_value' => @strval(intval($package->time_alotment))
      ]);

      //Assign options
      $event->makeDefaultOptions();

      return response()->json([
        'success' => true,
        'data' => $event->makeArray('details')
      ]);
    }

    function getUserIdFromIdentifier($email, $phone){
      $user = Users::where('email', $email)->orWhere('phone', $phone);
      if ($user->exists()){
        $user = $user->first();
        return $user['uid'];
      }else{
        return 0;
      }
    }

    public function configureInvite($eventId, Request $request){
      $subject = $request->input('subject');
      $body = $request->input('message');
      $event = Events::find($eventId);

      Invites::create([
        'uid' => Auth::user()->id,
        'event' => $eventId,
        'title' => $subject,
        'description' => $body
      ]);

      return response()->json([
        'success' => true
      ]);
    }

    public function inviteGuests($eventId, $request){
      $contacts = $request->input('contacts');
      $event = Events::find($eventId);
      $guests = $contacts;

      $tmp = array();

      foreach ($guests as $guest) {
        if (isset($guest['identifier'])){
          $email = $guest['identifier'];
          $phone = $guest['identifier'];
        }else{
          $email = (is_null($guest['email']) ? $guest['phone'] : $guest['email']);
          $phone = (is_null($guest['phone']) ? $guest['email'] : $guest['phone']);
        }

        if ($email == '' && $phone == '' && $guest['name'] == ''){ continue; }


        if (!$event->_guests()->where('identifier', $email)->orWhere('identifier', $phone)->exists()){
          $userId = $this->getUserIdFromIdentifier($email, $phone);
          if ($userId != 0){
            $user = User::find($userId);
            $user->notify(new \App\Notifications\GuestInvite($user, $event));
            if (!($event->_guests()->where('uid',$userId)->exists())){
              $event->_guests()->create([
                'identifier' => (is_null($email) ? $phone : $email),
                'uid' => $userId,
                'name' => $guest['name'],
                'status' => 0
              ]);
            }
          }else{
            $event->_guests()->create([
              'identifier' => (is_null($email) ? $phone : $email),
              'uid' => 0,
              'name' => $guest['name'],
              'status' => 0
            ]);
          }


        }
      }

      return response()->json([
        'success' => true
      ]);
    }

    public function respondToInvite($eventId, $response, Request $request){
      $user = Auth::user();

      $event = Events::find($eventId);
      $participants = $event->_participants;

      foreach ($participants as $participant) {
        if ($participant->uid == $user->uid){
          $participant->status = $response;
          $participant->save();
        }
      }

      $guests = $event->_guests;

      foreach ($guests as $guest) {
        if ($guest->uid == $user->id){
          $guest->status = $response;
          $guest->save();
        }
      }

      $eventUser = $event->_user;
      $eventUser->notify(new \App\Notifications\InviteResponse($user, $response));

      return response()->json([
        'success' => true,
        'data' => Events::find($eventId)->makeArray('details')
      ]);
    }

    public function getEventPrivacy($eventId, $privacy = null){
      $event = Events::find($eventId);

      if (is_null($event)){
        return response()->json([
          'success' => false,
          'data' => array(

          ),
          'error' => array(
            'title' => 'Invalid Event',
            'message' => 'That event does not exist. Enter another event id and try agian.'
          )
        ]);
      }

      if (!is_null($privacy)){
        $event->updateOption('privacy', ($privacy == 1 ? 0 : 1));
        return response()->json([
          'success' => true,
          'data' => array(
            'public' => $event->privacy()
          )
        ]);
      }else{
        if ($event->privacy()){
          return response()->json([
            'success' => true,
            'data' => array(
              'public' => $event->privacy()
            )
          ]);
        }else{
          return response()->json([
            'success' => false,
            'data' => array(
              'public' => $event->privacy()
            ),
            'error' => array(
              'title' => 'Event Private',
              'message' => 'This event is private, and you have not been invited.'
            )
          ]);
        }
      }
    }

    public function addParticipant($eventId, Request $request){
      if ($request->has('user')){
        $user = $request->input('user');

        $event = Events::find($eventId);

        if (is_null($event)){
          return response()->json([
            'success' => false,
            'data' => array(),
            'error' => array(
              'title' => 'No event exists',
              'message' => 'You have to provide a valid event.'
            )
          ]);
        }

        if (isset($user['identifier'])){
          $email = $user['identifier'];
          $phone = $user['identifier'];
        }else{
          $email = (is_null($user['email']) ? $user['phone'] : $user['email']);
          $phone = (is_null($user['phone']) ? $user['email'] : $user['phone']);
          $name = ((!empty($user['name'])) ? $user['name'] : 'John Doe');
        }

        $participants = $event->_participants()->create([
          'identifier' => $email,
          'name' => $name,
          'uid' => $this->getUserIdFromIdentifier($email, $phone),
          'role' => 'unknown',
          'status' => 0
        ]);
      }else{
        return response()->json([
          'success' => false,
          'data' => Events::find($eventId)->makeArray('details'),
          'error' => array(
            'title' => 'No participant provided',
            'message' => 'You have to provide a participant.'
          )
        ]);
      }

      return response()->json([
        'success' => true,
        'data' => Events::find($eventId)->makeArray('details')
      ]);
    }

    public function updateParticipant($eventId, $participantId, Request $request){
      // $user = $request->input('user');
      //
      // $event = Events::find($eventId);
      //
      // if (isset($user['identifier'])){
      //   $email = $user['identifier'];
      //   $phone = $user['identifier'];
      // }else{
      //   $email = (is_null($user['email']) ? $user['phone'] : $user['email']);
      //   $phone = (is_null($user['phone']) ? $user['email'] : $user['phone']);
      //   $name = (is_null($user['name']) ? $user['name'] : '');
      // }
      //
      // $participants = $event->_participants()->create([
      //   'identifier' => $email,
      //   'name' => $name,
      //   'uid' => $this->getUserIdFromIdentifier($email, $phone),
      //   'role' => 'unknown',
      //   'status' => 0
      // ]);
      $user = Users::find(Auth::user()->uid);
      $event = Events::find($eventId);
      $participants = $event->_participants;
      foreach($participants as $participant){
        $role = EventRoles::find($request->input('role'));
        if ($participant->role == 'creator') { continue; }
        $participant->role = $role->name;
        $participant->save();
      }

      return response()->json([
        'success' => true,
        'data' => Events::find($eventId)->makeArray('details')
      ]);
    }

    public function removeParticipant($eventId, $participantId, Request $request){
      $user = Users::find(Auth::user()->uid);
      $event = Events::find($eventId);
      $participants = $event->_participants;
      foreach($participants as $participant){
        $role = EventRoles::find($request->input('role'));
        if ($participant->role == 'creator') { continue; }
        //$participant->role = $role->name;
        //$participant->save();
      }
      return response()->json([
        'success' => true,
        'data' => Events::find($eventId)->makeArray('details')
      ]);
    }

    public function updateGuests($eventId, Request $request){
      $contacts = $request->input('contacts');
      if (sizeof($contacts) == 0){
        return response()->json([
          'success' => false,
          'data' => Events::find($eventId)->makeArray('details'),
          'error' => array(
            'title' => 'No guests provided',
            'message' => 'You must provide guests to add to your event.'
          )
        ]);
      }else{
        $this->inviteGuests($eventId, $request);
      }

      return response()->json([
        'success' => true,
        'data' => Events::find($eventId)->makeArray('details')
      ]);
    }

    public function startEvent($eventId, $userId){
      $openTok = new OpenTokController();
      return response()->json([
        'success' => true,
        'data' => array(
          'opentok' => $openTok->create($eventId, $userId),
          'numberOfParticipants' => sizeof(Events::find($eventId)->_participants)
        )
      ]);
    }

    public function stopEvent($eventId, $userId){
      $openTok = new OpenTokController();
      $openTok->stop($eventId, $userId);
      return response()->json([
        'success' => true,
        'data' => array()
      ]);
    }

    public function connectionCreated(Request $request){
      $json = Request::json();
      // $json = '{
      //     "sessionId": "2_MX4xMzExMjU3MX5-MTQ3MDI1NzY3OTkxOH45QXRr",
      //     "projectId": "123456",
      //     "event": "connectionCreated",
      //     "timestamp": 1470257688309,
      //     "connection": {
      //         "id": "c053fcc8-c681-41d5-8ec2-7a9e1434a21e",
      //         "createdAt": 1470257688143,
      //         "data": "TOKENDATA"
      //     }
      // }';
      //
      // $json = json_decode($json);

      // $sessionId = $json->input('sessionId');
      // $option = new Event_options;
      // $option = $option->where('option_value', $sessionId);
      // if ($option->exists()){
      //   $option = $option->first();
      //   $event = $option->event;
      //   $event->updateOption('total_viewers', strval(intval($event->getOption('total_viewers')) + 1));
      //   $event->updateOption('current_viewers', strval(intval($event->getOption('current_viewers')) + 1));
      // }
    }

    public function connectionDestroyed(Request $request){
      $json = json_decode($request->getContent(), true);
      // return dd($json);
      // $json = '{
      //     "sessionId": "2_MX4xMzExMjU3MX5-MTQ3MDI1NzY3OTkxOH45QXRr",
      //     "projectId": "123456",
      //     "event": "connectionDestroyed",
      //     "reason": "clientDisconnected",
      //     "timestamp": 1470258896953,
      //     "connection": {
      //         "id": "c053fcc8-c681-41d5-8ec2-7a9e1434a21e",
      //         "createdAt": 1470257688143,
      //         "data": ""
      //     }
      // }';
      //
      // $json = json_decode($json);

      // $sessionId = $json->input('sessionId');
      // $option = new Event_options;
      // $option = $option->where('option_value', $sessionId);
      // if ($option->exists()){
      //   $option = $option->first();
      //   $event = $option->event;
      //   $event->updateOption('current_viewers', strval(intval($event->getOption('current_viewers')) - 1));
      // }
    }

    public function streamCreated(Request $request){
      if ($request->getContent() == ''){ return; }
      $json = '{
          "sessionId": "2_MX4xMzExMjU3MX5-MTQ3MDI1NzY3OTkxOH45QXRr",
          "projectId": "123456",
          "event": "streamCreated",
          "timestamp": 1470258860571,
          "stream": {
              "id": "63245362-e00e-4834-8371-9397deb3e452",
              "connection": {
                  "id": "c053fcc8-c681-41d5-8ec2-7a9e1434a21e",
                  "createdAt": 1470257688143,
                  "data": ""
              },
              "createdAt": 1470258845416,
              "name": "",
              "videoType": "camera"
          }
      }';

      $json = json_decode($json);
    }

    public function streamDestroyed(Request $request){
      if ($request->getContent() == ''){ return; }
      $json = '{
          "sessionId": "2_MX4xMzExMjU3MX5-MTQ3MDI1NzY3OTkxOH45QXRr",
          "projectId": "123456",
          "event": "streamDestroyed",
          "reason": "clientDisconnected",
          "timestamp": 1470258896953,
          "stream": {
              "id": "63245362-e00e-4834-8371-9397deb3e452",
              "connection": {
                  "id": "c053fcc8-c681-41d5-8ec2-7a9e1434a21e",
                  "createdAt": 1470257688143,
                  "data": ""
              },
              "createdAt": 1470258845416,
              "name": "",
              "videoType": "camera"
          }
      }';

      $json = json_decode($json);
    }

    public function archiveGenerated(Request $request){
      if ($request->getContent() == ''){ return; }
      $json = json_decode($request->getContent(), true);

      $sessionId = $json->input('sessionId');
      $option = new Event_options;
      $option = $option->where('option_value', $sessionId);
      if ($option->exists()){
        $option = $option->first();
        $event = $option->event;
        $event->status = 2;
        $event->save();
      }
    }

    public function getArchive($eventId, Request $request){
      $openTok = new OpenTokController();
      return response()->json(array(
        'success' => true,
        'video' => $openTok->getArchive($eventId)
      ));
    }

    public function getMinutes($id, Request $request){
      return response()->json(array(
        'success' => true,
        'minutes' => intval(Events::find($id)->getOption('remaining_time'))
      ));
    }

    public function getEmojis(Request $request){
      return response()->json(array(
        'success' => true,
        'data' => array(
          'images' => Emojis::all()
        )
      ));
    }

    public function streamEvent(Request $request){
      return view('stream', []);
    }

}
