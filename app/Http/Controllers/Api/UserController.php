<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Users;
use Avatar;

class UserController extends Controller
{

  /**
   * getUser returns the profile for a given user.
   * @param  number   $id   The public ID for a user.
   * @return Response       JSON Response
   */
  public function getUser($id){
    $user = new Users;
    $user = $user->where('id','=',$id);
    if ($user->exists()){
      $user = $user->first();
      return response()->json($user->getProfile());
    }
  }

  public function getNotifications($id){
    $user = new Users;
    $user = $user->where('id','=',$id);
    if ($user->exists()){
      $user = $user->first();
      return response()->json($user->getNotifications());
    }
  }

  public function getAvatar($id){
    $user = new Users;
    $user = $user->where('id','=',$id);
    if ($user->exists()){
      $user = $user->first();
      $imgstr = Avatar::create($user->first_name . ' ' . $user->last_name)->__toString();
      $new_data=explode(";",$imgstr);
      $type=$new_data[0];
      $data=explode(",",$new_data[1]);
      header("Content-type:".$type);
      echo base64_decode($data[1]);
    }
  }
}
