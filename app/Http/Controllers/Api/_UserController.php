<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Users;
use Avatar;

class UserController extends Controller
{

  /**
   * getUser returns the profile for a given user.
   * @param  number   $id   The public ID for a user.
   * @return Response       JSON Response
   */
  public function getUser($id){
    $user = new Users;
    $user = $user->where('id','=',$id);
    if ($user->exists()){
      $user = $user->first();
      return response()->json($user->getProfile());
    }
  }

  public function getNotifications($id){
    $user = new Users;
    $user = $user->where('id','=',$id);
    if ($user->exists()){
      $user = $user->first();
      return response()->json($user->getNotifications());
    }
  }

  public function getAvatar($id){
    $user = new Users;

    if (!is_numeric($id)){
      $imgstr = Avatar::create(explode(' ', $id)[0] . ' ' . explode(' ', $id)[1])->__toString();
      $new_data=explode(";",$imgstr);
      $type=$new_data[0];
      $data=explode(",",$new_data[1]);
      header("Content-type:".$type);
      echo base64_decode($data[1]);
    }else{
      $user = $user->where('id','=',$id);
      if ($user->exists()){
        $user = $user->first();
        $imgstr = Avatar::create($user->first_name . ' ' . $user->last_name)->__toString();
        $new_data=explode(";",$imgstr);
        $type=$new_data[0];
        $data=explode(",",$new_data[1]);
        header("Content-type:".$type);
        echo base64_decode($data[1]);
      }
    }
  }

  public function listOfficiants(Request $request){
    return response()->json([
      'success' => true,
      'data' => array(
        'officiants' => array(
          array(
            'personal' => array(
              'id' => 1,
              'avatar' => '',
              'name' => array(
                'first' => 'John',
                'last' => 'Doe',
                'full' => 'John Doe'
              ),
              'location' => array(
                'address' => '2067 Hailston Dr',
                'city' => 'Duluth',
                'state' => 'GA',
                'zip' => 30097
              ),
              'email' => 'email@email.com',
              'phone' => 6784678435
            ),
            'officiant' => array(
              'prefix' => 'pastor',
              'description' => 'Primarily serving the Southeast.',
              'questions' => array(
                array(
                  'question' => 'Do you service areas outside of your hometown?',
                  'answer' => 'I do but not outside the USA'
                )
              ),
              'fee' => 50,
              'availability' => array(
                'mon'=>true,
                'tues'=>true,
                'wed'=>false,
                'thurs'=>true,
                'fri'=>false,
                'sat'=>true,
                'sun'=>true
              )
            )
          )
        )
      )
    ]);
  }
}
