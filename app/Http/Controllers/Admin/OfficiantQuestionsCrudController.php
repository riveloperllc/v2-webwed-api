<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\OfficiantQuestionsRequest as StoreRequest;
use App\Http\Requests\OfficiantQuestionsRequest as UpdateRequest;

class OfficiantQuestionsCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\OfficiantQuestions');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/officiantquestions');
        $this->crud->setEntityNameStrings('Question', 'Officiant Questions');
        // $this->crud->denyAccess(['create']);
        $this->crud->denyAccess(['delete']);
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $this->crud->setFromDb();

        $this->crud->addField([
            'name' => 'question',
            'label' => "Question",
            'type' => 'text',
            'wrapperAttributes' => [
               'class' => 'form-group col-md-12'
             ]
        ]);

        $this->crud->addField([
            'name' => 'status',
            'fake' => true,
            'label' => "Status",
            'type' => 'select_from_array',
            'options' => [0 => 'Inactive', 1 => 'Active'],
            'wrapperAttributes' => [
               'class' => 'form-group col-md-12'
             ]
        ]);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
