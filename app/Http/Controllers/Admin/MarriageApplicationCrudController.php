<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\MarriageApplicationRequest as StoreRequest;
use App\Http\Requests\MarriageApplicationRequest as UpdateRequest;

class MarriageApplicationCrudController extends CrudController
{
    public $tabs = [ 'Documents', 'History' ];
    public function setup()
    {


        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\MarriageApplication');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/marriageapplication');
        $this->crud->setEntityNameStrings('Marriage Application', 'Marriage Applications');
        $this->crud->denyAccess(['add', 'create', 'delete']);

        // $this->crud->setFromDb();
        //
        $this->crud->setColumns([
          [
              'name'  => 'last_name',
              'label' => 'Last',
              'type' => 'model_function',
              'function_name' => 'getUserLastName'
          ],
            [
                'name'  => 'event',
                'label' => 'First',
                'type' => 'model_function',
                'function_name' => 'getUserFirstName'
            ],
            [
                'name'  => 'created_at',
                'label' => 'Applied On',
                'type' => 'model_function',
                'function_name' => 'getFormattedDate'
            ],
            [
                'name'  => 'updated_at',
                'label' => 'Updated On',
                'type' => 'model_function',
                'function_name' => 'getFormattedDate'
            ],
            [
                'name'  => 'id',
                'label' => 'Status',
                'type' => 'model_function',
                'function_name' => 'getStatus'
            ],
        ]);

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->data['entry'] = $this->crud->getEntry($id);

        $manageCourt = '
        <div class="modal fade" id="courtModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3>Select a Court</h3>
              </div>
              <div class="modal-body">
                <p>Type the courts name, email address, or phone number.</p>
                <input type="text" class="form-control" />
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        ';

        $primaryContact = json_decode('{
	"personal": {
		"details": {
			"name": "John Doe",
			"birthday": "1/1/1980",
			"phone": "8552672777",
			"address": "2067 hailston dr",
			"city": "duluth",
			"state": "GA",
			"race": "White"
		},
		"parents": {
			"father": {
				"name": "",
				"birthplace": "",
				"address": ""
			},
			"mother": {
				"name": "",
				"birthplace": "",
				"address": ""
			}
		}
	},
	"spouse": {
		"details": {
			"name": "Jane Doe",
			"birthday": "1/1/1980",
			"phone": "8552672777",
			"address": "2067 hailston dr",
			"city": "duluth",
			"state": "GA",
			"race": "White"
		},
		"parents": {
			"father": {
				"name": "",
				"birthplace": "",
				"address": ""
			},
			"mother": {
				"name": "",
				"birthplace": "",
				"address": ""
			}
		}
	},
	"marriage": {
		"scheduled_date": "",
		"city": "",
		"county": "",
		"applicant_1_surname": "",
		"applicant_2_surname": "",
		"applicant_1_maiden": "",
		"applicant_2_maiden": "",
		"relation": ""
	}
}', true);


        $contactInformationHtml = '<div class="text-right">
        <h4>Respond – Under Review</h4>
        <button class="btn btn-success btn-lg">Approved</button>
        <button class="btn btn-danger btn-lg">Declined</button>
        <button onclick="event.preventDefault();" class="btn btn-warning btn-lg" data-toggle="modal" data-target="#myModal">Need More Information</button>
        </div>
        <hr/>';

        $contactInformationHtml .= '

        <div class="row">
          <div class="col-md-3">
            <h2><b>Court</b></h2>
          </div>
          <div class="col-md-9">
            <h2><a href="'.url("admin/courts/1/edit").'">Gwinnett County & Admin</a></h2>
          </div>
        </div>
        ';

        $contactInformationHtml .= '
        <div class="row">';

        $contactInformationHtml .= '<div class="col-md-6"><h3>Applicant 1</h3>';
        foreach($primaryContact['personal']['details'] as $key => $detail){
          $contactInformationHtml .= '
            <div class="row">
              <div class="col-md-2">
                <b>'.title_case($key).'</b>
              </div>
              <div class="col-md-10">
                '.title_case($detail).'
              </div>
            </div>';
        }
        $contactInformationHtml .= '</div><div class="col-md-6"><h3>Applicant 2</h3>';
        foreach($primaryContact['spouse']['details'] as $key => $detail){
          $contactInformationHtml .= '
            <div class="row">
              <div class="col-md-2">
                <b>'.title_case($key).'</b>
              </div>
              <div class="col-md-10">
                '.title_case($detail).'
              </div>
            </div>';
        }
        $contactInformationHtml .= '</div>';
        $contactInformationHtml .= '

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Change application status?</h4>
      </div>
      <div class="modal-body">
       You are about to change the status of this application. This action can’t be undone. You will only be permitted to view this page and will be prevented from making additional changes once submitted. Please confirm.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Confirm</button>
      </div>
    </div>
  </div>
</div>
        </div><hr/><h3><a target="_blank" href="'.url('/uploads/MARRIGE_APPLICATION.PDF').'" class="btn btn-primary btn-lg">View Marriage Application</a></h3>

        <table class="table datatable">
          <thead>
            <tr>
              <th>Document</th>
              <th>File</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Birth Certificate</td>
              <td><a href="#">Download</a></td>
            </tr>
            <tr>
              <td>Government ID</td>
              <td><a href="#">Download</a></td>
            </tr>
            <tr>
              <td>Divorce Decree</td>
              <td><a href="#">Download</a></td>
            </tr>
            <tr>
              <td>Proof of Residency</td>
              <td><a href="#">Download</a></td>
            </tr>
            <tr>
              <td>Blood Test</td>
              <td><a href="#">Download</a></td>
            </tr>
            <tr>
              <td>Marriage Education</td>
              <td><a href="#">Download</a></td>
            </tr>

          </tbody>
        </table>';

        $this->crud->addField([
          'name' => 'id2',
          'fake' => true,
          'type' => 'custom_html',
          'value' => $contactInformationHtml,

        ]);

        $this->crud->addField([
          'name' => 'id3',
          'fake' => true,
          'type' => 'custom_html',
          'value' => '

          <h3>History</h3>
          <table class="table datatable">
            <thead>
              <tr>
                <th>Status</th>
                <th>Last Updated</th>
              </tr>
            </thead>
            <tbody>
            <tr>
              <td>Under Review</td>
              <td>'.date('m/d/Y h:i A').'</td>
            </tr>
              <tr>
                <td>Submitted</td>
                <td>'.date('m/d/Y h:i A').'</td>
              </tr>


            </tbody>
          </table>
          ',

        ]);

        //$this->crud->addField((array) json_decode($this->data['entry']->field)); // <---- this is where it's different
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;

        $this->data['id'] = $id;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
