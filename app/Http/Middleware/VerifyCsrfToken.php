<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
    ];
}

use Closure;
class CORS {

/**
* Handle an incoming request.
*
* @Param \Illuminate\Http\Request $request
* @Param \Closure $next
* @return mixed
*/
public function handle($request, Closure $next)
{

header("Access-Control-Allow-Origin: *");

// ALLOW OPTIONS METHOD
$headers = [
'Access-Control-Allow-Methods'=> 'POST, GET, OPTIONS, PUT, DELETE',
'Access-Control-Allow-Headers'=> 'Content-Type, X-Auth-Token, Origin'
];
if($request->getMethod() == "OPTIONS") {
// The client-side application can set only headers allowed in Access-Control-Allow-Headers
return Response::make('OK', 200, $headers);
}

$response = $next($request);
foreach($headers as $key => $value)
$response->header($key, $value);
return $response;
}

}
