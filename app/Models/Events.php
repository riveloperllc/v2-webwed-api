<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use App\Models\Users;
use App\Models\Images;
use App\Models\Participants;
use App\Models\OpenTokController;

class Events extends Model
{
    use CrudTrait;

     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    //protected $table = 'eventss';
    protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    public $type = 'event';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getUserProfile() {
        $user = $this->_user;
        return '<a href="'.url("admin/user/".$user->id."/edit").'">' . $user->first_name . ' ' . $user->last_name .'</a>';
    }

    public function formatDate(){
      return date('m/d/Y h:i A', $this->scheduled_date);
    }

    public function getUserProfile2() {
        $user = $this->user;

        return $user->first_name . ' ' . $user->last_name;
    }

    // public function getPackageLink() {
    //     // $package = $this->package;
    //     return '<a href="'.url("admin/packages/1/edit").'">15 Minutes</a>';
    // }

    public function getParticipants(){
      $tmpParticipants = $this->_participants;
      $participants = array();
      foreach ($tmpParticipants as $participant) {
        $user = $participant->_user;
        if (is_null($user)){
          $participants = array_merge($participants, array(
            array('personal' => array(
                  'id' => 0,
                  'avatar' => url(env('APP_URL').'api/user/'.@explode(' ', $participant->name)[0].'_'.@explode(' ', $participant->name)[1].'/avatar'),
                  'name' => array(
                    'first' => @explode(' ', $participant->name)[0],
                    'last' => @explode(' ', $participant->name)[1],
                    'full' => $participant->name
                  )
                )
              )
            )
          );
        }else{
          $participants = array_merge($participants, array($user->getPublicProfile()));
        }
      }

      return $participants;
    }

    public function getPhoto(){
      $images = Images::find('event'.$this->id);
      if ($images){
        return $images->value;
      }else{
        return 'assets/images/Placeholder.svg';
      }
    }

    /**
     * Makes array of public properties depending on type supplied.
     * If the type is minimal, basic information is supplied,
     * detailed provides a more indepth look at an event.
     * @param  string $type Minimal, or Detailed (Lowercase)
     * @return array       Array containing properties returned.
     */
    public function makeArray($type = 'minimal', $isInvite = false){
      if ($isInvite){
        $typeOfEvent = 'invite';
      }else{
        $typeOfEvent = 'event';
      }
      // return dd($this);
      $details = array(
        'id' => $this->id,
        'date' => ($this->scheduled_date),
        'participants' => $this->getParticipants(),
        'photo' => $this->getPhoto(),
        'title' => $this->title,
        'type' => $typeOfEvent
      );

      if ($typeOfEvent == 'invite'){
        $details['status'] = (new Participants)->getStatusForEvent($user, $event->id);
      }

      if ($type == 'details'){
        $details['package'] = array(
          'name' => '30 Minutes',
          'remaining_balance' => 20,
          'used_balance' => 10
        );
        $details['premium'] = array(
          array(
            'id' => 'registry',
            'name' => 'Link to a Registry',
            'value' => 'http://google.com'
          )
        );

        $users = array();

        $allParticipants = array();
        $participants = $this->_participants;

        foreach ($participants as $participant) {
          if (!is_null($participant->_user)){
            $userId = $participant->_user->id;
          }else{
            $userId = $participant->id;
          }
          //@TODO implement status, and last response for participant replies.
          $allParticipants = array_merge($allParticipants, array(
              array(
                "user" => $userId,
                "status" => $participant->status,
                "role" => $participant->role,
                "last_response" => strtotime('now')
              )
            )
          );

          if (!is_null($participant->_user)){
            $users[$userId] = $participant->_user->getPublicProfile();
          }else{
            $users[$userId] = array('personal' => array(
                'id' => $participant->id,
                'avatar' => url(env('APP_URL').'api/user/'.@explode(' ', $participant->name)[0].'_'.@explode(' ', $participant->name)[1].'/avatar'),
                'name' => array(
                 'first' => @explode(' ', $participant->name)[0],
                 'last' => @explode(' ', $participant->name)[1],
                 'full' => $participant->name
                )
              ));
          }
        }

        return array(
          'details' => $details,
          'participants' => $allParticipants,
          'guest' => $this->_guests,
          'users' => $users
        );
      }else{
        return $details;
      }
    }

    public function privacy(){
      $eventOptions = $this->_options;
      foreach($eventOptions as $option){
        if ($option['option_key'] == 'privacy'){
          return boolval($option['option_value']);
        }
      }

      return false;
    }

    public function sessionId(){
      $eventOptions = $this->_options;
      foreach($eventOptions as $option){
        if ($option['option_key'] == 'session_id'){
          return ($option['option_value']);
        }
      }

      return (new OpenTokController)->generateSessionId();
    }

    public function participantNumber($userId){
      $user = new Users();
      $user = $user->where('id','=',$userId);
      if ($user->exists()){
        $user = $user->first();
        $participants = $this->_participants;
        $index = 1;
        foreach($participants as $participant){
          if ($participant->uid == $user->uid){
            return 'participant'.$index;
          }

          $index += 1;
        }
      }else{
        return 'participant0';
      }
    }

    public function updateOption($key, $value){
      $eventOptions = $this->_options;
      foreach($eventOptions as $option){
        if ($option['option_key'] == $key){
          $option['option_value'] = $value;
          $option->save();
        }
      }
    }

    public function getOption($key){
      $eventOptions = $this->_options;
      foreach($eventOptions as $option){
        if ($option['option_key'] == $key){
          return $option['option_value'];
        }
      }
    }

    public function makeDefaultOptions(){
      $this->_options()->create([
        'option_key' => 'privacy',
        'option_value' => '1'
      ]);
      $this->_options()->create([
        'option_key' => 'total_viewers',
        'option_value' => '0'
      ]);

      $this->_options()->create([
        'option_key' => 'current_viewers',
        'option_value' => '0'
      ]);
      $sessionId = (new OpenTokController)->generateSessionId();
      $this->_options()->create([
        'option_key' => 'session_id',
        'option_value' => $sessionId
      ]);
      $this->_options()->create([
        'option_key' => 'archive_id',
        'option_value' => (new OpenTokController)->generateArchiveId($sessionId)
      ]);
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function _user() {
      return $this->hasOne('App\Models\Users', 'id'); //Not correct it is using ID not user_id
    }

    public function _package() {
      return $this->hasOne('App\Models\Packages', 'id', 'package');
    }

    public function _participants() {
      return $this->hasMany('App\Models\Participants', 'event', 'id');
    }

    public function _guests() {
      return $this->hasMany('App\Models\Guests', 'event', 'id');
    }

    public function _options() {
      return $this->hasMany('App\Models\Event_options', 'event', 'id');
    }

    public function chat() { }

    public function _saleRecord() {
      return $this->hasOne('App\Models\Event_sales', 'event', 'id');
    }

    public function _addonSalesRecords() {
      return $this->hasMany('App\Models\Preimum_addon_sales', 'event', 'id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
