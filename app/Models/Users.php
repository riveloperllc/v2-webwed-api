<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Auth;
use App\Models\UserOptions;
use App\Models\Events;
use App\Models\Guests;
use Illuminate\Notifications\Notifiable;

use App\Notifications\GuestInvite;
use App\Notifications\InviteResponse;
use App\Notifications\OfficiantMissing;


class Users extends Model
{
    use CrudTrait;
    use Notifiable;

     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'firebase_users';
    protected $primaryKey = 'uid';
    public $timestamps = true;
    public $incrementing = false;
    // protected $guarded = ['id'];
    protected $fillable = [ 'first_name', 'last_name', 'email', 'phone', 'token' ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

   public function getUserProfile(){
     return '<a href="#">User</a>';
   }

   public function isMyAccount($id){
     if ($this->id == $id){
       return true;
     }else{
       return false;
     }
   }

   public function getEvents($user = null, $wantsPast = false) {
     if ($user == null){
       $user = $this;
     }

     $data = array();

     foreach ($user->participants as $participant) {
       $event = $participant->_event;

       if ($participant->role == 'creator'){
         $typeOfEvent = 'event';
       }else{
         $typeOfEvent = 'invite';
       }

       if (strtotime('now') > $event->scheduled_date){
         $past = true;
       }else{
         $past = false;
       }

       if ($past == $wantsPast || $wantsPast == null){
         $details = array(
             "id" => $event->id,
             "photo" => $event->getPhoto(),
             "title" => $event->title,
             "date" => ($event->scheduled_date),
             "past" => $past,
             "participants" => $event->getParticipants(),
             "type" => $typeOfEvent,
             "event_status" => $event->status
           );
         if ($typeOfEvent == 'invite'){
           $details['status'] = (new Participants)->getStatusForEvent($user, $event->id);
         }
         $data = array_merge($data, array($details));
       }
     }

     foreach ($user->guests as $guest) {
       $event = Events::find($guest->event);
       $typeOfEvent = 'invite';


       if (strtotime('now') > $event->scheduled_date){
         $past = true;
       }else{
         $past = false;
       }

       if ($past == $wantsPast || $wantsPast == null){
         $details = array(
             "id" => $event->id,
             "photo" => $event->getPhoto(),
             "title" => $event->title,
             "date" => ($event->scheduled_date),
             "past" => $past,
             "participants" => $event->getParticipants(),
             "type" => $typeOfEvent,
             "event_status" => $event->status
           );
         if ($typeOfEvent == 'invite'){
           $details['status'] = (new Guests)->getStatusForEvent($user, $event->id);
         }
         $data = array_merge($data, array($details));
       }
     }

     usort($data, function($a, $b) {
          return $b['date'] - $a['date'];
     });

     return $data;
   }

   public function getPublicProfile() {
     $personal = array(
       'id' => $this->id,
        'avatar' => url(env('APP_URL').'/api/user/'.$this->id.'/avatar'),
        'name' => array(
          'first' => $this->first_name,
          'last' => $this->last_name,
          'full' => $this->first_name . " " . $this->last_name
        )
    );

     return array(
       'personal' => $personal
     );
   }

   public function getProfile() {
     $requestingUser = Auth::user();

     $personal = $this->getPublicProfile()['personal'];

    // return dd($this->participants->_event);


    if ($requestingUser == null){
      $requestingUid = -1;
    }else{
      $requestingUid = $requestingUser->uid;
    }

     if ($requestingUid == $this->uid){
       $personal['email'] = $this->email;
       $personal['dob'] = UserOptions::find($this->id)->dob();
       $personal['phone'] = $this->phone;
       $personal['location'] = UserOptions::find($this->id)->address();
       $personal['privacy'] = UserOptions::find($this->id)->privacy();

       return array(
         'personal' => $personal,
         'events' => $this->getEvents($this, null)
       );
     }else{
       return array(
         'personal' => $personal
       );
     }
   }

   public function getNotifications() {
     $requestingUser = Auth::user();
     $requestedUser = $this;

     if ($requestingUser->id == $requestedUser->id){
       return array(
         'notifications' => array(
           array(
             'id' => 1,
             'avatar' => 'assets/icon/favicon.ico',
             'title' => 'Account Activated',
             'message' => 'Your WebWed Account is now active.',
             'timestamp' => strtotime('now'),
             'status' => 'unread'
           )
         )
       );
     }
   }

   //@TODO problem updating options
   public function updateOption($key, $value){
     $userOptions = $this->options;
     if (sizeof($userOptions) == 0){ return; }
     foreach($userOptions as $option){
       if ($option['option_key'] == $key){
         $option['option_value'] = $value;
        //  $option->save();
       }
     }
   }

   public function makeDefaultOptions($data){
     $this->options()->create([
       'option_key' => 'address',
       'option_value' => '{"address": "3410 Cobblestone Ct","city": "Loganville","state": "GA","zip": "30548"}'
     ]);
     $this->options()->create([
       'option_key' => 'dob',
       'option_value' => '0'
     ]);
     $this->options()->create([
       'option_key' => 'email_notifications',
       'option_value' => '1'
     ]);
     $this->options()->create([
       'option_key' => 'push_notifications',
       'option_value' => '1'
     ]);
   }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function participants(){
      return $this->hasMany('App\Models\Participants', 'uid', 'uid');
    }

    public function guests(){
      return $this->hasMany('App\Models\Guests', 'uid', 'id');
    }

    public function orders(){
      return $this->hasMany('App\Models\Event_sales', 'uid', 'uid');
    }

    public function options(){
      return $this->hasMany('App\Models\UserOptions', 'uid', 'id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
