<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Packages extends Model
{
    use CrudTrait;

     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    //protected $table = 'packagess';
    //protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getTimeAlotment(){
      return $this->time_alotment . ' Minutes';
    }

    public function getPrice(){
      return '$'.$this->price;
    }

    public function getTitle($time){
      if ($time > 60){
        $hours = round($time/60);
        return $hours . ' Hour(s)';
      }else{
        return $time . ' Minute(s)';
      }
    }

    public function getPackages(){
      $packages = $this->all();
      $allPackages = array();
      foreach ($packages as $package){
        $time_alotment = $package->time_alotment;
        $title = $this->getTitle($time_alotment);
        $allPackages = array_merge($allPackages, array(
          array(
            'id' => $package->id,
            'title' => $title,
            'price' => $package->price
          )
        ));
      }
      return $allPackages;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
