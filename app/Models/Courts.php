<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Courts extends Model
{
    use CrudTrait;

     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    //protected $table = 'courtss';
    //protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

   public function generateProcessingTable(){
     $table = '<table class="table datatable">
       <thead>
         <tr>
           <th>User</th>
           <th>Type</th>
           <th>Status</th>
           <th>Last Updated</th>
         </tr>
       </thead>
       <tbody>';
      foreach($this->getApplications as $application){
        // return dd($application);
        $table .= '
            <tr>
              <td><a href="'.url('admin/user/'.$application->_event->_user->id.'/edit').'">'.$application->_event->_user->first_name.' '.$application->_event->_user->last_name.'</a></td>
              <td>Marriage Application</td>
              <td>'.$application->_status().'</td>
              <td>'.date('m/d/Y h:i A', strtotime($application->created_at)).'</td>
              <td><a href="'.url('admin/marriagelicense/1/edit').'">View</a></td>
            </tr>';
      }
      $table .= '
         </tbody>
      </table>';

    return $table;
   }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

   public function getApplications(){
     return $this->hasMany('App\Models\Marriage_applications', 'court', 'id');
   }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
