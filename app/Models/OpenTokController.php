<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OpenTok\OpenTok;
use OpenTok\MediaMode;
use OpenTok\ArchiveMode;
use OpenTok\Session;
use OpenTok\Role;
use OpenTok\OutputMode;
use App\Models\Events;


class OpenTokController extends Model
{
    private $apiKey;
    private $apiSecret;
    private $opentok;

    public function __construct(){
      $this->apiKey = getenv('OPENTOK_API_KEY');
      $this->apiSecret = getenv('OPENTOK_SECRET');
      // return dd($this->apiSecret);
      $this->opentok = new OpenTok($this->apiKey, $this->apiSecret);
    }

    public function generateSessionId(){
      $sessionOptions = array(
          'mediaMode' => MediaMode::ROUTED
      );
      $session = $this->opentok->createSession($sessionOptions);
      return $session->getSessionId();
    }

    public function generateArchiveId($sessionId){
      $archiveOptions = array(
          'name' => 'Example Event',     // default: null
          'hasAudio' => true,                     // default: true
          'hasVideo' => true,                     // default: true
          'outputMode' => OutputMode::COMPOSED  // default: OutputMode::COMPOSED
      );
      //$archive = $this->opentok->startArchive($sessionId, $archiveOptions);
      return '';
      // return $archive->id;
    }

    public function create($eventId, $userId){
      // Store this sessionId in the database for later use
      $event = Events::find($eventId);

      $token = $this->opentok->generateToken($event->sessionId(), array(
          'role'       => Role::MODERATOR,
          'expireTime' => time()+(7 * 24 * 60 * 60), // in one week
          'data'       => $event->participantNumber($userId)
      ));

      return array(
        'session_id' => $event->sessionId(),
        'token' => $token,
        'role' => $event->participantNumber($userId)
      );
    }

    public function getArchive($eventId){
      $event = Events::find($eventId);
      $archiveId = $event->getOption('archive_id');
      $archive = $this->opentok->getArchive($archiveId);

      return $archive->toArray()['url'];
    }

    public function stop($eventId,$userId){

    }
}
