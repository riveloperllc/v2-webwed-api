<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Marriage_applications extends Model
{
    public function _event() {
      return $this->hasOne('App\Models\Events', 'id', 'event');
    }

    public function _court() {
      return $this->hasOne('App\Models\Courts', 'id', 'court');
    }

    public function _status(){
      return 'Pending Review';
    }

    public function _application() {
      return json_decode($this->application_information, true);
    }
}
