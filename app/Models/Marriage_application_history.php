<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Marriage_application_history extends Model
{
    public function _application() {
      return $this->hasOne('App\Models\Marriage_applications', 'id', 'application');
    }
}
